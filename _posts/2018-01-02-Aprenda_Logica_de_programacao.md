---
layout: post
title: "APRENDENDO LÓGICA DE PROGRAMAÇÃO"
description: "Entender Lógica de programação ou seja não aprendeu a base da programação."
date: 2018-01-02 10:39:18
comments: true
image: https://proudsalopian.files.wordpress.com/2014/10/phptutorials.jpg
description: "Entender Lógica de programação ou seja não aprendeu a base da programação."
keywords: "APRENDENDO LÓGICA DE PROGRAMAÇÃO"
category: Programação
tags:
- welcome
---

<p>APRENDENDO LÓGICA DE PROGRAMAÇÃO</p>



Olá, sou Hemershon Silva e resolvi escrever um post sobre lógica de programação devido minha experiência com vários programadores que já vão direto pra linguagem sem saber o quão valioso é entender o que é a base da programação.

No primeiro passo, vamos falar do ínicio e do conceito da Lógica de programação, até porque tudo tem um começo, surge por algum motivo, e sem saber como toda a engrenagem funciona, você não entenderá nada sobre um software a não ser que seja um ninja, hehe.

A história dos algoritmos se confunde com a história da computação. O computador na verdade é uma máquina, coitada, sem qualquer tipo de inteligência.

Mas o que as tornam tão poderosas, rápidas e temidas? Sua espantosa velocidade de fazer contas matemáticas.
<div align="center"><img src="http://s2.glbimg.com/vFDVZ8g6LyLOB_LdutPckPQ3Uoo=/e.glbimg.com/og/ed/f/original/2014/02/20/equation.jpg
"></div>


<br>
<br>
A lógica da programação é um assunto muito grande e complexo, vamos dar apenas uma introdução a história da lógica da programação.

<h4>Resumo</h4>
A primeira pessoa a pensar em usar lógica matemática para a programação foi John McCarthy, ele propôs usar programas para manipular com sentenças instrumentais comuns apropriadas à linguagem formal, ou seja, o programa básico formará conclusões imediatas a partir de uma lista de premissas essas conclusões serão tanto sentenças declarativas quanto imperativas. Quando uma sentença imperativa é deduzida, o programa toma uma ação correspondente.<br>
Mais foi a <b>Ada Augusta Byron King</b> Condessa de Lovelace á primeira programadora, a escreve o primeiro programa de computador do mundo em 1843 para ser utilizado na máquina analítica de Charles Babbage.
<img src="https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAKRAAAAJDU3MDE0ZDViLWJhNTAtNGE1ZS05NjQ3LTc3ZDVjYzY0ZGVkZg.jpg" alt="maquina"><br>
<i><br>
"Ela foi a primeira maquina que poderia ser programada para executar varios comandos de qualquer tipo, ela funcionava com base nas instruções de cartões perfurados e era movida a vapor".</i>

<p>Existe uma associação direta da Lógica de Programação com o Raciocínio Matemático, onde o importante é a interpretação de um problema e a utilização correta de uma fórmula. De fato, não existe “fórmulas” em informática, o que existe é nosso modo de pensar em como resolver e extrair o máximo de informações de um problema, de maneira eficaz e eficiente sobre um ângulo de visão. Essa solução precisa ser exteriorizada e expressa numa linguagem conhecida. A lógica da programação entra nesse ponto, para desenvolvermos soluções e algorítimos para apresentar essa solução ao mundo.</p>
<p>
</p>

<p><strong><img src="/images/plankaljul.jpg" alt="maquina" width="540" height="300" align="right"><i>A primeira linguagem de alto nível do mundo foi a <b>Plankalkül</b>, criada pelo cientista alemão Konrad Zuse, entre os anos 1942-1946 no desenvolvimento dos primeiros computadores. Ela é considerada de alto nível porque em termos simples, ela é mais “humana” e está longe do código de máquina, se aproximando mais da linguagem humana. Vamos entender melhor o que é uma linguagem de alto nível e a diferença dela e a de máquina.</i></strong><br></p>

<p>
Uma pessoa que não entende nada de programação, mas sabe inglês, entenderia algumas palavras da linguagem e poderia facilmente interpretar alguma coisa desse código em PASCAL. “Program” é programa em inglês, “begin” é começar, “write” é escrever e end é fim. Mesmo sem saber programar, a pessoas que sabe um pouco de inglês chegaria a mais ou menos a essa conclusão: “É um programa e vai começar a escrever alguma coisa e depois terminar”. E é justamente isso!</p>
<img src="https://vignette.wikia.nocookie.net/borlandpascal/images/5/54/Hello_world.png/revision/latest?cb=20071210155316" width="640" height="400" align="center"><br>
Quando nos falamos de linguagem mais humana é justamente pelo fato de a entendermos melhor, de ser humanamente mais fácil de se compreender. Você facilmente entenderia, com um pouco de estudo, esse código.

<h2>Mais afinal, O que é Lógica de Programação?</h2>



Lógica de Programação nada mais é que desenvolver algoritmos com sequências lógicas para atingir determinados objetivos dentro de uma certas regras baseadas na lógica da matemática e em outras teorias básicas da Ciência da Computação e que depois são adaptadas para a linguagem de Programação, que você irá usar para construir um software


Simples não? Agora você deve estar com uma dúvida sobre uma outra palavra que você ouvi bastante no mundo da programação, Algoritmo.


<h2>E o que é Algoritmo?</h2>


Um algoritmo é nada mais que uma sequência não ambígua de instruções que é executada até determinada condição se verifique.

Especificamente, em matemática, constitui o conjunto de processos e símbolos que para efetuar um cálculo.

Ex:
<img src="https://image.slidesharecdn.com/aula2raciocniolgicoaplicadoaresoluo-120321120311-phpapp01/95/raciocnio-lgico-aplicado-a-resoluo-de-problemas-matemticos-7-728.jpg?cb=1332333369" width="640" height="400" align="center"><br>

<ul>
<i>
<h5>Sobre isso, vários doutrinadores conceituaram o termo, e aqui vai alguns para complementar seu aprendizado.<br></h5>


"Algoritmo é uma seqüência de passos que visam atingir um objetivo bem definido, (FORBELONE,1999).<br>


Algoritmo é a descrição de uma seqüência de passos que deve ser seguida para a

realização de uma tarefa, (ASCENCIO, 1999).<br>


Algoritmo é uma seqüência finita de instruções ou operações cuja a execução, em tempo finito, resolve um problema computacional, qualquer que seja sua instância, (SALVETTI, 1999)<br>


Algoritmo são regras formais para a obtenção de um resultado ou da solução de um problema, englobando fórmulas de expressões aritméticas, (MANZZANO,1997)<br>



Ação é um acontecimento que, a partir de um estado inicial, após um período de tempo finito, produz um estado final previsível e bem definido. Portando um algoritmo é a descrição de um conjunto de comandos que, obedecidos, resultam numa sucessão finita de ações”(FARRER,1999 )"</i>

</ul>

Diferentes algoritmos podem realizar a mesma tarefa usando um conjunto diferenciado de instruções em mais ou menos tempo, espaço ou esforço do que outros. Tal diferença pode ser reflexo da complexidade computacional aplicada, que depende de estruturas de dados adequadas ao algoritmo.


Como se faz a representação de Algoritmos? Há Várias formas de representação de algoritmos, mais não tem consenso com relação a melhor delas.<br>


<h3>As formas mais conhecidas que posso citar:</h3>

<b>Descrição: narrativa-Fluxograma convencional-Português estruturado ou Portugol.</b>

<li>Tipos de dados e instruções primitivas;
<li>Tipos de Informações;<br>



<p>Todo computador tem uma função de solucionar problemas que envolve a manipulação de informações, classificam-se em duas categorias básicas: DADOS E INSTRUÇÕES;</p>


<p><h2>TIPOS DE DADOS</h2></p>


Os dados são representados pelas informações a serem processadas por um computador.<br>

<p>Identificados por 4 tipos de dados:
<ul>
 <li>DADOS NUMÉRICOS INTEIROS
 <li>DADOS NUMÉRICOS REAIS
 <li>DADOS CARACTERES E DADOS LÓGICOS;
 <p>
 <p>
 <p>
<i>Primeiro exemplo;</i>


<h3>TIPO INTEIROS</h3>

São os dados numéricos positivos ou negativos, excluindo-se destes qualquer numérico fracionário.<br>
<p>
EX: 33, 120, 0<br>



<h3>TIPOS REAIS</h3>
<p>

São os dados numéricos positivos, negativos e números fracionários.<br>
<p>

 Ex: 33.0, -120, 0, 2.34,-43.679


<h3>TIPOS DE CARACTERES</h3>
<p>


São as sequências contendo letras, números, e símbolos especiais, indicadas entres aspas <b>("")ou('')</b>.<br>
<p>

 EX: "HEMERSHON SILVA", 'MACAPÁ'
<p>

<h3>TIPOS LÓGICOS</h3>


São os dados com valores verdadeiro e falso, sendo que este tipo de dado poderá representar apenas um dos dois valores. Deve ser indicado entre pontos.<br>


EX: Falso. ( exemplo falso); Verdadeiro.(exemplo verdadeiro)<br>
<p>

<p>


Precisa-se entender também a CONSTATE, que é nada mais, nada menos que, tudo aquilo que é fixo ou estável, vou explicar melhor. As constantes são campos cujos valores são definidos em tempo de compilação e nunca podem ser alterados.<br>


Em contrapartida, a VARIÁVEL é tudo aquilo que é sujeito a variações, que é incerto, instável e inconstante.


<h3>FORMAÇÃO DE IDENTIFICADORES</h3>


É o nome de uma variável, que representa sempre valores em mutação. Esses nomes devem acompanhar as seguintes regras de formação:


Devem começar por um caracter alfabético;

Podem ser seguidos por mais caracteres alfabéticos e/ou numérico;

Não é permitido o uso de caracteres especiais.

Os caracteres alfabéticos devem obrigatoriamente ser escritos em maiúsculo.<br>
<p>
<p>



Ex: <b>NOME_CLI , ENDAL , NUM1 , NOTA1 , A4 , WW2</b>


<h3>DECLARAÇÃO DE VARIÁVEL</h3>


No ambiente computacional, as informações variáveis são guardadas em dispositivo eletrônicos analogamente chamados de “memória”.<br>

Na memória (armário) existem inúmeras variáveis (gavetas), precisamos diferenciá-las, o que é feito por meio de identificadores (etiquetas). Cada variável (gaveta), no entanto, pode guardar apenas uma informação (objeto) de cada vez, sendo sempre do mesmo tipo (material).<br>

Não podemos permitir que mais de uma variável (gaveta) possua o mesmo identificador (etiqueta), ficaríamos sem saber que gaveta abrir. Só podemos guardar informações (objeto) em variáveis (gavetas) do mesmo material (tipo primitivo).<br>

<p>
<p>

<h3> OPERADORES ARITMÉTICOS</h3>


Chamamos de operadores aritméticos o conjunto de símbolos que representa as operações básicas da matemática, a saber:<br>

Operador:<br>

Adição+multiplicação*potenciação^subtração–divisão  /raiz(x)<br>

<p>
<p>

Ex:: 2+2 , XPTO/5 , X^2 , X – 3 , 3^2 , raiz (x) , 2*NOTA , raiz (9)
<p>


OPERANDO!!<br>



Usaremos outras operações matemáticas não convencionais cujos nomes dos operadores são:
<p>

mod (resto da divisão )

div (quociente da divisão inteira )
<p>

Ex: 15mod2 = 1 9div2 = 4<br>
<p>



<h3> FUNÇÕES MATEMÁTICAS</h3>


Podemos usar nas expressões aritmética algumas funções da matemática:<br>
<ul>
<li>Abs (x) – valor absoluto (módulo) x

<li>Int (x) – a parte inteira de um número fracionário

<li>frac (x) – a parte fracionária de x

<li>arred (x) – transforma, por arredondamento, um número fracionário em inteiro<br>


Onde x pode ser um número, variável expressão aritmética ou também outra função matemática.<br>
<p>
<p>

Exemplos:<br>
<p>
<ul>

<li>Int (34,866) resulta 34

<li>frac (34,866) resulta 866

<li>ard ( 34,866) resulta 35

<li>ard ( 34,336) resulta 34

<li>abs (-27) resulta 27<br>


<p>
<p>
PRIORIDADES<br>
<p>


parêntese mais interno

^ raiz (x)

* / div mod

+ –
<p>


<h3>OPERADORES RELACIONAIS</h3>


Utilizamos os operadores relacionais para realizar comparações entre dois valores de mesmo tipo primitivo. Tais valores são representados por constantes, variáveis ou expressão aritmética.<br>
Os operadores relacionais são:<br>
=
igual a
< >
diferente de
>

maior que

> =

maior ou igual a

<

menor que

< =

menor ou igual <br>
<p>




<h3>OPERADORES LÓGICOS</h3>


Os operadores lógicos são:<br>


<ul>
<li>.ou - torna a sentença verdadeira se pelo menos uma condição for verdadeira

<li>.e. – torna a sentença verdadeira se todas as condições envolvidas forem verdadeiras.

<li>.não.	– faz a negativa da condição.


<h3> PRIORIDADE</h3>

<ul>
<li>não.

<li>e.

<li>ou.<br>



Dentre todos os operadores:<br>

<ul>
<li>parênteses mais interno

<li>funções matemáticas

<li>operadores aritméticos

<li>operadores relacionais

<li>operadores lógicos
</ul>

<h3>COMANDO DE ATRIBUIÇÃO</h3>


Um comando de atribuição permite-nos fornecer um valor a uma certa variável (guardar um objeto numa certa gaveta), onde o tipo dessa informação deva ser compatível com o tipo da variável, somente podemos atribuir um valor lógico a uma variável do tipo lógico.<br>


<p>
<p>
Exemplo:<br>
<p>
<ul>
<li>declare A lógico;

<li>X numérico;

<li>A ¬ VERDADEIRO;

<li>X ¬ 8=3 div 5;


<p>
<h3> INSTRUÇÕES</h3>

São representadas pelo conjunto de palavras-chaves (vocabulário) de uma determinada linguagem de programação.<br>
 Adotamos uma pseudolinguagem denominada portugol ou português estruturado.<br>
<ul>
ENTRADA E SAÍDA DE DADOS ( português estruturado ou portugol )


ENTRADA DE DADOS<br>

leia X ;<br>

leia ( A, XPTO, NOTA );<br>
<p>
<p>
SAÍDA DE DADOS<br>
<p>
escreva (x);<br>

escreva (B, XPTO, MEDIA );<br>

escreva (“Bom Dia”, NOME);<br>

escreva (“ VOCÊ PESA” , x , “quilos”);<br>
