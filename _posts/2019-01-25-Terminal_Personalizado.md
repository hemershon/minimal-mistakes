---
layout: post
title: "Deixando seu terminal mas aprensetavel para seu Desenvolvimento"
description: "O Oh My Zsh é uma estrutura orientada a comunidade e de código aberto para gerenciar sua configuração de zsh ."
date: 2019-01-25 22:49:18
comments: true
image: https://camo.githubusercontent.com/5c385f15f3eaedb72cfcfbbaf75355b700ac0757/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6f686d797a73682f6f682d6d792d7a73682d6c6f676f2e706e67
description: "O Oh My Zsh é uma estrutura orientada a comunidade e de código aberto para gerenciar sua configuração de zsh."
keywords: "ZSH deixe seu terminal elegante"
category: linux
tags:
- welcomeasdfasdfasdsadfsadf
---

# Oh My Zsh
  

 Zsh é um shell de login interativo que também pode ser usado como um
   poderoso intérprete de linguagem de script. É semelhante ao bash e ao
   Korn shell, mas oferece uma grande capacidade de configuração.



  

#### Instalar o ZSH

    sudo apt install zsh curl git
  

#### Instalar o Oh My ZSH

 `sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`


![instalação concluída](https://lh3.googleusercontent.com/Whj7Oq9k0ZoPj0a0i2vWQ8Ne8FPgoWVSN2HQu4yybwqhrNKpnPT1O5wJPMkBzNLIFzh5m07JYJ_a7Q "Instalado")

### Instalando Tema

  

Nesse [link](https://github.com/robbyrussell/oh-my-zsh/wiki/themes) tem os temas do oh-my-zsh. Para mudar de tema é preciso entrar no arquivo _.zshrc_ e alterar a linha `ZSH_THEME="robbyrussell"` pelo tema que preferir.

  



## Referência

  

### OhMyZSH

https://github.com/robbyrussell/oh-my-zsh

