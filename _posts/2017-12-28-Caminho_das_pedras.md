---
title: "Layout: Header Video"
image: https://proudsalopian.files.wordpress.com/2014/10/phptutorials.jpg
header:
  video:
    id: 
    provider: youtube
categories:
  - Layout
  - Uncategorized
tags:
  - video
  - layout
---

<b>Você se impressiona  com a explosão do mercado com tantos app ou melhor tanta novidade e inovação, deu vontade de começa na programação?.</b>

Saiba que  iniciar na tecnologia é muito "difícil" sem uma base sólida.<br>

Um grande professor me parou certo dia e disse que tecnologias vem e vão, mais a base fica, ele me contou uma história de um amigo de faculdade que era sempre bem curioso com essas novas tecnologias, aprendendo todas as linguagens, um dia este cara foi concorrer a uma vaga para um determinado emprego devido seu grau de instrução de uma determinada tecnologia, só que além dessa tecnologia veio um desafio básico para ele, tipo aqueles que um iniciante daria conta de resolver, e o rapaz não conseguiu devido não ter uma base sobre o assunto, mais como um Doutor não tem uma base sobre DBA?.<br>

É meus amigos o rapaz é doutor, e ele não conseguiu a vaga devido não possuir uma base sólida de conhecimento sobre DBA. A questão aqui é se você quer realmente aprender a programar não vá do A para o Z rapidamente, não pule etapas, aprenda a base que você entenderá tudo que vier pela frente.<br>

Todos que querem iniciar na programação aprendam realmente como um  algoritmo funciona, poderia perder muito tempo tentando explicar como e porque a lógica é importante para um programador mais a ideia é apenas mostrar pra você que todas as disciplinas têm um motivo um objetivo, por isso estão na grade curricular.<p>
 Já vi, várias pessoas que trabalham na área e que não entendem um algoritmo ou não entendem a lógica de alguma coisa, o maior exemplo disso sou eu, vou ser franco, foi preciso eu voltar aos livros e a faculdade para eu poder entender como ambas funcionam, a maioria dos “programadores” não aprendem ou acham que aprendendo uma linguagem vão entender tudo ou algo do tipo.<br> Vejo na rede vários tutoriais ou vídeo aulas explicando uma linguagem ou uma ferramenta que o desenvolvedor vai precisar, ou algo como dicas ou como desenvolver deve usa uma Distribuição nova.<p>
  O segredo é: antes de ir para o degrau final suba os degraus iniciais, li em um post <a href="https://medium.com/@lfeh/15-coisas-que-faria-se-estivesse-iniciando-minha-carreira-como-desenvolvedor-9fdd0c17b473">Felipe Fialho</a> de que me estimulou a escrever, cujo título era: 15 coisas que faria se estivesse iniciando minha carreira como desenvolvedor,
<h2>Usarei aqui o mesmo argumento</h2>
<ul>
<li>Aprenderia inglês
<li>Aprenderia lógica de programação
<li>Escolheria entre Back-end ou Front-end
<li>Buscaria seguir as referências da profissão
<li>Focaria nas linguagens básicas
<li>Aprenderia uma coisa por vez
<li>Contribuiria em projetos open-source de terceiros
<li>Entenderia tudo que abrange meu ecossistema
<li>Iria em eventos
<li>Escutaria os profissionais mais experientes
<li>Teria minha própria opinião
<li>Evitaria fazer freelas
<li>Tomaria cuidado com a síndrome do impostor
<li>Estipularia metas
<li>Não deixaria de ter hobbies ou praticar atividades físicas
<li>Compartilharia meus conhecimentos
<li>acredite apenas isso já lhe daria uma ótima base para trabalhar bem motivado!
<br>
<br>





<h2>Fato Curioso!</h2>
Quem não tem a curiosidade de saber quem foi o pioneiro na Programação?<br>

O primeiro programador foi uma mulher, <b>Ada Augusta Byron King</b>, a Condessa de Lovelace escreveu o primeiro programa de computador do mundo em 1843 para ser utilizado na máquina analítica de Charles Babbage.<br>

Surpreso? A Máquina de Babbage foi apresentada como proposta (já que era difícil de ser construída) em 1833, patrocinada pela Universidade de Cambridge, da qual, em 1823, Babbage ganhara uma bolsa para conceber uma calculadora com capacidade para até a vigésima casa decimal.<br>
O algoritmo criado por Ada calculava a sequência de Bernoulli, conhecida também como a Lei dos Grandes Números. Segundo esse teorema, caso se conheça a probabilidade de ocorrência de um evento num experimento aleatório, será possível indicar quais são as expectativas da frequência da sua ocorrência uma vez que o mesmo experimento seja repetido um número considerável de vezes sob condições semelhantes. Cara ou coroa, por exemplo.<br>

<img src="http://www.techtudo.com.br/platb/files//2196/2011/06/techtudo-babbage-ada-lovelace-machine-Bernoulli.jpg" alt="maquina">
