---
layout: post
title: "RVM Ruby on Rails Debian 9"
description: "Ambiente de desenvolvimento web Ruby on Rails para Debian 9 "
date: 2018-08-10 21:49:18
comments: true
image: https://www.hugeserver.com/kb/wp-content/uploads/2017/05/CoM_Stuk.io-Ruby-on-Rails1.jpg
description: "Ambiente de desenvolvimento web Ruby on Rails para Debian 9,"
keywords: "RVM Ruby on Rails Debian 9"
category: linux
tags:
- welcome
---
<h2> Ambiente de desenvolvimento web Ruby on Rails para Debian 9.</h2>


<p>Olá, antes de começar o tutorial vou contar uma pequena experiência nessa instalação de ambiente.<p>
Quando comecei com ruby on rails  alguns anos atrás sofri bastante com a preparação deste ambiente, minhas primeiras tentativas de configuração foram horríveis, sempre dava erro, sem falar que existem milhões de tutoriais explicando como configurar esse ambiente e nenhum tinha feedback, até mesmo agora quando troquei de computador  precisei correr atrás de um tutorial para criar esse ambiente.<p> Então não se preocupe se não conseguir de primeira ou se você não entendeu algo, meu e-mail está à disposição para suas dúvidas, vou explicar de uma forma simples e bem didática, espero que gostem!!!

Para funcionar o Ruby on Rails em nossas máquinas precisamos configurar com algumas dependências que fazem com que RoR possa funcionar perfeitamente em sua máquina e ajudar muito no seu dia dia, mas não estranhe quando se deparar com outras dependências, pois ao longo do tutorial vou explicando as funcionalidades. .<p>

<p><h4>Instalando o Ruby on rails com dependências necessárias
Antes, você precisa atualizar seu Debian</h4>
<iframe width="358" height="200" aling="center" src="https://www.youtube.com/embed/Ts8sOXNOZDY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>

<code> sudo apt update</code><p>
<code>sudo apt upgrade</code><p>
<p>

Isso vai atualizar seu sistema e deixar ele redondo para o resto das instalações<p>

Próximo passo é para o git.<p>
<code> sudo apt install git</code><p>
<h4>O que é git?</h4>

Git é um sistema de controle de versão de arquivos e através deles podemos desenvolver projetos nos quais diversas pessoas podem contribuir simultaneamente no mesmo projeto, e como seria? Editando e criando novos arquivos e permitindo que os mesmos possam existir sem o risco de suas alterações serem subescritas.<p>
<h4>após a instalação vamos para configuração</h4><p>
<code>git config ---global user.name "seu nome"</code><p>
<code>git config –global user.email "seu email"</code><p>
essa configuração é para o git entender de quem é os commits e se tem autorização.
Caso você não entendeu, vou falar mais sobre git na próximo tutorial.<p>

Antes de instalar o nodejs precisamos do repositório<p>
<code>sudo curl -sSL https://deb.nodesource.com/setup_8.x | sudo bash -</code><p>
<h4>agora vamos a instalação</h4><p>
<code>sudo apt install nodejs</code><p>
<h3>O que é nodejs?</h3>
Node.js é uma plataforma construída sobre o motor JavaScript do Google Chrome para facilmente construir aplicações de rede rápidas e escaláveis. Node.js usa um modelo de I/O direcionada a evento não bloqueante que o torna leve e eficiente, ideal para aplicações em tempo real com troca intensa de dados através de dispositivos distribuídos.<p>


<h3>O que é Yarn</h3>

É um gerenciador ( dependências) de pacotes javascript que podemos via linha de comandos, baixar pacotes para nossa aplicação que foram desenvolvidos por outras pessoas, além de poder compartilhar também, além de ser uma alternativa ao npm.<p>

Agora vamos instalar o Yarn, como você vai ver aqui, essa instalação é um pouco complexa porque vai precisar de uma chave GPG<p>

<h3>O que é GPG?</h3><p>
GPG é uma criptografia assimétrica, ou seja, emprega um par de chaves para alcançar seu objetivo – uma chave pública e uma chave privada.<p>
<code>curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -</code><p>

também vamos adicionar o repositório do Yarn<p>

<code>echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list</code><p>

<h3>Finalmente vamos a instalação do yarn</h3><p>
<code>
sudo apt install yarn
</code>
<p>

O Yarn usa o mesmo package.json que já temos no projeto em JavaScript e não adiciona nenhuma outra dependência,utilizando o mesmo repositório que o NPM já roda.<p>
Qual a vantagem do Yarn?<p>
Uma das vantagens do Yarn é ele ser mais rápido, tende a resolver alguns conflitos de versão de módulo, cria um cash local para caso você já tenha utilizado algum módulo qualquer com Yarn, essa versão específica já fica salva na sua máquina e caso você precise usar em outro projeto, ela utiliza do cash e não precisa baixar um novo, além disso, resolve algumas coisas interessantes como, por exemplo, é muito comum fazermos:<p>

Agora vamos para o tão esperado rvm
<h3>O que é o RVM?</h3>
RVM é acrônimo para Ruby Version Manager, uma ferramenta de linha de comando que possibilita instalar e gerenciar diversas versões do Ruby e suas gems.<p>
Usar o RVM facilita a transição entre versões do Ruby e versões das gems.<p>
Usar o RMV é muito simples, o único requisito básico é um ambiente POSIX (linux, bsd, etc), nesse tutorial vou instalar no Mac, basta abrir o terminal e executar o comando abaixo:<p>

<h3>Sem mais delogas vamos direto para instalação</h3>
<code>\curl -sSL https://get.rvm.io | bash</code><p>
<h3>agora vamos dar um endereço para o seu bash executar o rvm no</h3>

<code>source ~/.rvm/scripts/rvm</code><p>


Source port é o nome dado à portabilização de um programa que é geralmente apenas um arquivo executável que foi modificado a partir do código de fonte do programa original e substituiu o executável original, para que possa ser rodado em uma outra plataforma para a qual não foi originalmente escrito.<p>

<h3>vou falar mais sobre rvm e suas versões no próximo tutorial, no momento vamos instalar essa versão</h3><p>
<code>rvm install 2.5.3</code>
<p>
<h3>Vamos usar essa versão</h3><p>
<code>rvm use 2.5.3</code><p>

<h3>Os Ideais do Criador do Ruby</h3>
O Ruby é uma linguagem com um cuidadoso equilíbrio.<p> 
O seu criador, Yukihiro “Matz” Matsumoto, uniu partes das suas linguagens favoritas (Perl, Smalltalk, Lisp e outros) para formar uma nova linguagem que equilibra a programação funcional com a programação imperativa.<br>
Ele disse com frequência que está “tentando tornar o Ruby natural, não simples”, de uma forma que reflita a vida.
Elaborando sobre isto, acrescenta:<br>
O Ruby é simples na aparência, mas muito complexo no interior, tal como o corpo humano.<p>


<h3>RubyGems</h3>

RubyGems é um gerenciador de pacotes para a linguagem de programação Ruby que provê um formato padrão para a distribuição de programas Ruby e bibliotecas em um formato auto-suficiente chamado de gem ("jóia", do inglês), uma ferramenta projetada para gerenciar facilmente a instalação de gems, e um servidor para distribuí-los.<p>
 RubyGems foi criado em volta de novembro de 2003 e agora faz parte da biblioteca padrão do Ruby versão 1.9 a diante.<p>

<h3>Redis</h3>
 Isso deve ser novo pra você certo ou não?<p>

vamos usar o redis nesse ambiente nos próximos tutoriais, vou me aprofundar nesse banco não relacional,

a instalação dele é meia complexa, mais é só seguir meus passos que você vai conseguir

Redis: o que é, e para que serve?

um banco de dados não relacional, também conhecido por NOSQL (Not Only SQL), que foi criado por Salvatore Sanfiippo e liberado em forma open-source em 2009.
Redis significa REmote DIctionary Server, e pelo seu significado, já podemos ter uma ideia de como ele trabalha e armazena os dados. Os dados são armazenados na forma de chave-valor, lembrando a estrutura do Dictionary do .net e do Map do Java. Um ponto importante que vale chamar a atenção aqui, é que o valor utilizado como chave no Redis pode possuir diferentes formatos, podendo ser strings, hashes, lists, sets e sets ordenados.<p>
<h3>vai fazer o download<h3><p>
<code>
wget https://download.redis.io/redis-stable.tag.gz
</code>
<p>

logo em seguida vamos descompactar o aquivo baixado<p>
<code>
tar xvzf redis-stable.tar.gz
</code><p>

você precisa entrar no diretório da pasta redis-stable

<p>
<code>
cd redis-stable
</code>
<p>

já dentro da pasta vamos a instalação<p>

<code>
make
</code>
<p>
<code>
make test
</code>
<h3>Aguarde...&#9749;</h3>
<p>
<code>
sudo make install
</code>
<p>
tudo certo?
<p>

Agora vamos executar
<p>
<code>
redis-server –daemonize yes
</code>
<p>


O Redis é extremamente rápido, tanto para escrita como para leitura dos dados, graças ao fato de armazenar seus dados em memória. Apesar disso, o Redis permite que os dados sejam persistidos fisicamente caso desejado.<p>


<h3>Agora vamos para a instalação do postgresql</h3><p>

bom vou usar esse banco de dados para nossas aulas, mais caso queira usar outro banco fique a vontade. Por padrão o Rails usa o SQLite3 pois é bem simples, versátil e consegue se comportar bem em um ambiente de desenvolvimento.<p>

Instalação do postgresql<p>
<code>
sudo apt install postgresql
</code>
<p>
<h3>O que é o PostgreSQL?</h3><p>

O PostgreSQL é um sistema de gerenciamento de banco de dados objeto-relacional baseado no POSTGRES Versão 4.2 desenvolvido pelo Departamento de Ciência da Computação da Universidade da Califórnia em Berkeley. O POSTGRES foi pioneiro de vários conceitos que somente se tornaram disponíveis muito mais tarde em alguns sistemas de banco de dados comerciais.<br>

O PostgreSQL é um descendente de código fonte aberto deste código original de Berkeley, que suporta grande parte do padrão SQL e oferece muitas funcionalidades modernas, como:<br>
comandos complexos<p>
chaves estrangeiras<p>
gatilhos<p>
visões<p>
integridade transacional<p>
controle de simultaneidade multiversão<p>
Além disso, o PostgreSQL pode ser ampliado pelo usuário de muitas maneiras como, por exemplo, adicionando novos
tipos de dado<p>
funções<p>
operadores<p>
funções de agregação<p>
métodos de índice<p>
linguagens procedurais<p>
Devido à sua licença liberal, o PostgreSQL pode ser utilizado, modificado e distribuído por qualquer pessoa para qualquer finalidade, seja particular, comercial ou acadêmica, livre de encargos.<p>



<h3>Antes de irmos para outros passos precisamos instalar uma dependência da gem pg</h3><p>

<code>
sudo apt install libpq-dev
</code>

<p>
Bom para que funcione precisamos configura-lo para que o mesmo rode perfeitamente no seu ambiente,  antes de mais nada vamos verificar qual é a versão que foi instalado no seu ambiente,

 não importa qual é a sua versão que está instalado no seu ambiente, essa configuração vai funcionar em quase todas versões anteriores

essa configuração é um pouco complexa, mais você fazendo com calma e com atenção você consegue
primeiro vamos ao .conf


O pg_hba.conf controla as  que máquinas terão acesso ao PostgreSQL e a autenticação dessas máquinas clientes (sem autenticação ou através de outras formas, trust, md5, crypt, etc).<br>
Quando é especificada a autenticação trust, o PostgreSQL assume que qualquer um que possa se conectar ao servidor está autorizado a acessar o banco de dados como qualquer usuário que seja especificado (incluindo o superusuário do banco de dados).<br>
Os métodos de autenticação baseados em senha são md5, crypt e password. Estes métodos operam de forma semelhante, exceto com relação à forma como a senha é enviada através da conexão, mas somente o método md5 suporta senhas criptografadas armazenadas no catálogo do sistema.<br>
<h3>Vamos á alteração</h3>

<code>

sudo nano /etc/postgresql/9.6/main/pg_hba.conf  

</code>
<p>
<h3>Indentifique as linhas abaixo, com elas que vamos fazer á alteração</h3>

<pre class="code-pre ">
  
    # Database administrative login by Unix domain socket
   
    local        all       postgres              peer

</pre>

<h3>Altere para <code>trust</code></h3>

<pre class="code-pre ">

    # Database administrative login by Unix domain socket
    
     local        all      postgres                  trust

</pre>

pronto!<p>
<p>
<h3>Salva e vamos reiniciar o postgresql</h3><p>
<code>
/etc/init.d/postgresql restart ou service postgresql restart
</code>
<p>
<h3>vamos conectar</h3>
<code>
psql -U postgres
</code>
<p>

<h3>precisamos de uma senha para o usuário postgres certo?</h3><p>
<code>

ALTER USER postgres WITH ENCRYPTED PASSWORD ‘suasenha’;

</code>
<p>
agora vamos sair do postgres<p>

\q<p>

novamente vamos alterar o <code>socket</code><p>
<code>
sudo nano /etc/postgresql/9.6/main/pg_hba.conf  
</code>
<p>

<h3>indentifique as linhas abaixo, com elas que vamos fazer á alteração</h3>

<pre class="code-pre ">

    # Database administrative login by Unix domain socket
    
     local        all      postgres                  trust

</pre>
<h3>altere a para</h3>

<pre class="code-pre ">

    # Database administrative login by Unix domain socket
    
     local        all      postgres                  md5

</pre> 

pronto!<p>

<h3>Salva e vamos reiniciar o postgresql</h3><p>
<code>

/etc/init.d/postgresql restart ou service postgresql restart

</code>
<p>
<h3>Vamos acessar novamente o postgres</h3>
<code>
psql -U postgres -W
</code>

<h3>digite sua senha</h3>

<h3>pronto o usuario postgres está ok, agora vamos para seu usuário, o que você vai usar no seu ambiente</h3>
<code>
CREATE USER fulano WITH ENCRYPTED PASSWORD ‘senha’;
</code>
<p>
<h3>
observe que você criou seu usuário, agora vamos dar as permissões para ele como um super user
</h3>
<p>
<code>
ALTER USER fulano WITH SUPERUSER;
</code>
<p>

<h3>
Pronto seu postgresql está funcionando perfeitamente!
</h3>


<h2>Rails</h2>

<h3>O que é rails?</h3>

Ruby on Rails é um framework que faz o desenvolvimento, implantação e manutenção de uma aplicação web mais fácil e essa utiliza uma linguagem orientada à objeto conhecida como Ruby. Para introduzi-lo é preciso que o desenvolvedor conheça algumas de suas filosofias. Essas são:<br>

a instalação dele é bem simples<br>
<p>
<h3>verifique todas as versões do rails</h3><p>
<code>

gem search '^rails$' --all

</code>
<p>
<code>

gem install rails -v 5.2.0
</code>
<p>
pronto seu ambiente está todo configurado!<p>

Vamos testar?<p>

<h2>Vamos criar uma aplicação</h2><p>
<code>
rails new teste --database=postgresql
</code>
<p>
<code>
cd /teste
</code>
<p>
<code>

rails s
</code>